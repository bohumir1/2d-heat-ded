rho = 4420         # Density of material, kg/m3

# thermal parameters
ts = 1873
tl = 1923
tm = (tl+ts)/2.0
tr = 2*(tl-tm)

tcd = 7.2          # thermal conductivity of solid
tdiff = tcd        # thermal conductivity !!!
cp = 560           # specific heat of solid,  J/(kg*K)
convh =  30        # conv. heat transfer coefficient, W/K

lat = 3.65e5       # Latent heat of material, J/kg

eps = 0.54         # thermal emissivity
