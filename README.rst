2d-heat-ded
===========

Overview
--------

2d-heat-ded.py implements two-dimensional finite element model of heat transfer during the Directed Energy Deposition (DED) process. The model is implemented in Python using FEniCS framework (https://fenicsproject.org/pub/tutorial/html/ftut1.html) for solving PDEs in Python. The model incrementally deposits material ahead of the laser focus point according to the geometry of the part including three large circular voids. The laser heat energy is supplied by a Gaussian-distributed heat source while the phase change is represented by increased heat capacity around the solidus-liquidus temperature range. Numerical model is calibrated by matching results of the melt pool temperature measurements taken by a dual wavelength pyrometer during the build process of a box-shaped Ti-6Al-4V part with large spherical voids. Theoretical background of the numerical model, geometry of the part, and the physical buildng proces with in-situ pyrometer measurements used for calibration are described in the following reference: https://doi.org/10.1016/j.jmapro.2020.06.021.


Files in the package
--------------------

2d-heat-ded.py ... main program, generates/read-in mesh and simulates laser deposition

conf_proc.py ... configuration file for deposition process

conf_geom.py ... configuration file for geometry of a part

conf_mat.py ... configuration file for material properties

mesh.xml ... mesh generated for geometry in conf_geom.py, can be re-used in subsequent runs

Requirements
------------

FEniCS framework, 2d-heat-ded was tested with versions 2017.1.0 and 2019.1.0

Execution
---------

A) Generate mesh (what can take hours) and then simulate case 1

  python3 2d-heat-ded.py

B) Mesh generation is time consuming. Therefore, the mesh that could otherwise be generated in the step A) is included in the "mesh.xml" file which comes with the 2d-heat-ded package. To skip the mesh generation step, supply the included mesh as a command line parameter to model execution:

  python3 2d-heat-ded.py "mesh.xml"

The program will read the mesh from the "mesh.xml" file, and the computational time is only spent on the simulation of the deposition process.

Simulation results are written in HDF5 files along with XDMF files containing metadata. Use Visit or Paraview, which are open-source graphics packages, to read XDMF files and visualize results.

Input parameters
----------------

2d-heat-ded can simulate the three cases mentioned in the article out-of-the-box. The case is chosen by changing "conf_proc.py" code:

  case = 1   deposits with left-to-right direction
  
  case = 2   deposits with alternating directions
  
  case = 3   deposits with alternating directions, lower laser power, higher initial temperature

Authors
-------

Bohumir Jelinek <bj48@cavs.msstate.edu>

License
-------

LGPL-3.0

Acknowledgment
--------------

Research was sponsored by the Army Research Laboratory and was
accomplished under Cooperative Agreement Number W911NF-15-2-0025.
Cleared for "Public Release, Distribution Unlimited".
